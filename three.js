
window.onload = function() {
    // Build a system
    var url = window.location.search.match(/url=([^&]+)/);
    if (url && url.length > 1) {
      url = decodeURIComponent(url[1]);
    } else {
      url = window.location.origin;
    }
    var options = {
    "swaggerDoc": {
      "swagger": "2.0",
      "info": {
        "version": "2018-05-31T05:55:06Z",
        "title": "PetStore"
      },
      "host": "4vdbdi19j0.execute-api.us-east-1.amazonaws.com",
      "basePath": "/one",
      "schemes": [
        "https"
      ],
      "paths": {
        "/pets": {
          "get": {
            "produces": [
              "application/json"
            ],
            "parameters": [
              {
                "name": "type",
                "in": "query",
                "required": false,
                "type": "string"
              },
              {
                "name": "page",
                "in": "query",
                "required": false,
                "type": "string"
              }
            ],
            "responses": {
              "200": {
                "description": "200 response",
                "schema": {
                  "$ref": "#/definitions/Pets"
                },
                "headers": {
                  "Access-Control-Allow-Origin": {
                    "type": "string"
                  }
                }
              }
            },
            "security": [
              {
                "TestCongito": []
              }
            ]
          },
          "post": {
            "operationId": "CreatePet",
            "consumes": [
              "application/json"
            ],
            "produces": [
              "application/json"
            ],
            "parameters": [
              {
                "in": "body",
                "name": "NewPet",
                "required": true,
                "schema": {
                  "$ref": "#/definitions/NewPet"
                }
              }
            ],
            "responses": {
              "200": {
                "description": "200 response",
                "schema": {
                  "$ref": "#/definitions/NewPetResponse"
                },
                "headers": {
                  "Access-Control-Allow-Origin": {
                    "type": "string"
                  }
                }
              }
            },
            "security": [
              {
                "TestCongito": []
              }
            ]
          },
          "options": {
            "consumes": [
              "application/json"
            ],
            "produces": [
              "application/json"
            ],
            "responses": {
              "200": {
                "description": "200 response",
                "schema": {
                  "$ref": "#/definitions/Empty"
                },
                "headers": {
                  "Access-Control-Allow-Origin": {
                    "type": "string"
                  },
                  "Access-Control-Allow-Methods": {
                    "type": "string"
                  },
                  "Access-Control-Allow-Headers": {
                    "type": "string"
                  }
                }
              }
            }
          }
        },
        "/pets/{petId}": {
          "get": {
            "operationId": "GetPet",
            "produces": [
              "application/json"
            ],
            "parameters": [
              {
                "name": "petId",
                "in": "path",
                "required": true,
                "type": "string"
              }
            ],
            "responses": {
              "200": {
                "description": "200 response",
                "schema": {
                  "$ref": "#/definitions/Pet"
                },
                "headers": {
                  "Access-Control-Allow-Origin": {
                    "type": "string"
                  }
                }
              }
            },
            "security": [
              {
                "TestCongito": []
              }
            ]
          },
          "options": {
            "consumes": [
              "application/json"
            ],
            "produces": [
              "application/json"
            ],
            "parameters": [
              {
                "name": "petId",
                "in": "path",
                "required": true,
                "type": "string"
              }
            ],
            "responses": {
              "200": {
                "description": "200 response",
                "schema": {
                  "$ref": "#/definitions/Empty"
                },
                "headers": {
                  "Access-Control-Allow-Origin": {
                    "type": "string"
                  },
                  "Access-Control-Allow-Methods": {
                    "type": "string"
                  },
                  "Access-Control-Allow-Headers": {
                    "type": "string"
                  }
                }
              }
            }
          }
        }
      },
      "securityDefinitions": {
        "TestCongito": {
          "type": "apiKey",
          "name": "Authorization",
          "in": "header",
          "x-amazon-apigateway-authtype": "cognito_user_pools"
        }
      },
      "definitions": {
        "Pets": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Pet"
          }
        },
        "Empty": {
          "type": "object"
        },
        "NewPetResponse": {
          "type": "object",
          "properties": {
            "pet": {
              "$ref": "#/definitions/Pet"
            },
            "message": {
              "type": "string"
            }
          }
        },
        "Pet": {
          "type": "object",
          "properties": {
            "id": {
              "type": "string"
            },
            "type": {
              "type": "string"
            },
            "price": {
              "type": "number"
            }
          }
        },
        "NewPet": {
          "type": "object",
          "properties": {
            "type": {
              "$ref": "#/definitions/PetType"
            },
            "price": {
              "type": "number"
            }
          }
        },
        "PetType": {
          "type": "string",
          "enum": [
            "dog",
            "cat",
            "fish",
            "bird",
            "gecko"
          ]
        }
      }
    },
    "customOptions": {}
  };
    url = options.swaggerUrl || url
    var customOptions = options.customOptions
    var spec1 = options.swaggerDoc
    var swaggerOptions = {
      spec: spec1,
      url: url,
      dom_id: '#swagger-ui',
      deepLinking: true,
      presets: [
        SwaggerUIBundle.presets.apis,
        SwaggerUIStandalonePreset
      ],
      plugins: [
        SwaggerUIBundle.plugins.DownloadUrl
      ],
      layout: "StandaloneLayout"
    }
    for (var attrname in customOptions) {
      swaggerOptions[attrname] = customOptions[attrname];
    }
    var ui = SwaggerUIBundle(swaggerOptions)
  
    if (customOptions.oauth) {
      ui.initOAuth(customOptions.oauth)
    }
  
    if (customOptions.authAction) {
      ui.authActions.authorize(customOptions.authAction)
    }
  
    window.ui = ui
  }
  