const express = require('express')
const app = express()
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./shippo.json');
 
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.get('/', function(req, res){
res.send("swagger is at api-docs")
});

app.get('/one', function(req, res) {
    res.sendFile(__dirname + "/" + "one.js");
  });

  app.get('/two', function(req, res) {
    res.sendFile(__dirname + "/" + "two.js");
  });

  app.get('/three', function(req, res) {
    res.sendFile(__dirname + "/" + "three.js");
  });

  app.get('/four', function(req, res) {
    res.sendFile(__dirname + "/" + "four.css");
  });

app.listen((process.env.PORT || 8000), () => {
    console.log("Server is up and running...");
});